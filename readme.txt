=== Default Widgets Extended ===
Contributors: kasparsj@gmail.com
Donate link: https://www.paypal.com/cgi-bin/webscr?cmd=_s-xclick&hosted_button_id=53WLQWY9HJ4X4
Tags: Widget, Recent Posts, Pages, Search
Requires at least: 2.5
Tested up to: 3.5
Stable tag: 1.0
License: GPLv2 or later
License URI: http://www.gnu.org/licenses/gpl-2.0.html

Extends the default Recent Posts, Pages and Search widgets with more otions.

== Description ==

*Extends the default Recent Posts, Pages and Search widgets with more otions:*

*Recent Posts widget extended options:*
* Display post excerpt when available
* Display thumbnail
* Configure thumbnail size
* Choose the Post Type to display
* Display archive link

*Pages widget extended options:*
* Display content
* Display thumbnail
* Configure thumbnail size
* Choose the Post Type to display

*Search widget extended options:*
* placeholder

[Demo](http://www.osi.lv/)

== Installation ==

1. Upload `default-widgets-extended` directory to the `/wp-content/plugins/` directory
2. Activate the plugin through the 'Plugins' menu in WordPress

== Frequently asked questions ==

== Screenshots ==

1. Recent Posts widget
2. Pages widget
3. Search widget

== Changelog ==

1.0 Initial release

== Upgrade notice ==



