<?php

// Exit if accessed directly
if( !defined( 'ABSPATH' ) ) exit;

class Search_Widget extends WP_Widget_Search {
    
    protected $current_instance;
    
	function widget( $args, $instance ) {
		extract($args);
		$title = apply_filters( 'widget_title', empty( $instance['title'] ) ? '' : $instance['title'], $instance, $this->id_base );

		echo $before_widget;
		if ( $title )
			echo $before_title . $title . $after_title;
        
        $this->current_instance = $instance;
        add_filter('get_search_form', array($this, 'get_search_form'));

		// Use current theme search form if it exists
		get_search_form();
        
        remove_filter('get_search_form', array($this, 'get_search_form'));
        $this->current_instance = null;

		echo $after_widget;
	}
    
    function get_search_form($form = '') {
        if ($form && 
            isset($this->current_instance['placeholder']) && $this->current_instance['placeholder'] &&
            class_exists('DOMDocument') && function_exists('simplexml_import_dom')) {
            $doc = new DOMDocument();
            $doc->strictErrorChecking = false;
            $doc->loadHTML('<?xml encoding="UTF-8">' . $form);
            $form_xml = simplexml_import_dom($doc);
            $input = $form_xml->xpath('//input[@id="s"]');
            if ($input) {
                $input[0]->addAttribute('placeholder', $this->current_instance['placeholder']);
                return $form_xml->asXML();
            }
        }
        return $form;
    }

	function form( $instance ) {
		$instance = wp_parse_args( (array) $instance, array( 'title' => '') );
		$title = $instance['title'];
        $placeholder = $instance['placeholder'];
?>
		<p><label for="<?php echo $this->get_field_id('title'); ?>"><?php _e('Title:'); ?> 
        <input class="widefat" id="<?php echo $this->get_field_id('title'); ?>" name="<?php echo $this->get_field_name('title'); ?>" type="text" value="<?php echo esc_attr($title); ?>" /></label></p>
        
        <p><label for="<?php echo $this->get_field_id('placeholder'); ?>"><?php _e('Placeholder:'); ?> 
        <input class="widefat" id="<?php echo $this->get_field_id('placeholder'); ?>" name="<?php echo $this->get_field_name('placeholder'); ?>" type="text" value="<?php echo esc_attr($placeholder); ?>" /></label></p>
<?php
	}

	function update( $new_instance, $old_instance ) {
		$instance = $old_instance;
		$new_instance = wp_parse_args((array) $new_instance, array( 'title' => ''));
		$instance['title'] = strip_tags($new_instance['title']);
        $instance['placeholder'] = strip_tags($new_instance['placeholder']);
		return $instance;
	}

}

?>
