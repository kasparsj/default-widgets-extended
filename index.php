<?php

/*
Plugin Name: Default Widgets Extended
Plugin URI: http://github.com/kasparsj/default-widgets-extended
Description: Extends the default Recent Posts, Pages and Search widgets with more otions.
Version: 1.0
Author: Kaspars Jaudzems
Author URI: http://kasparsj.wordpress.com
Text Domain: osi
*/

// Exit if accessed directly
if( !defined( 'ABSPATH' ) ) exit;

function load_textdomain_widgets_extended() {
    load_plugin_textdomain('dwe', false, basename(dirname(__FILE__)).'/languages' );
}
add_action('plugins_loaded', 'load_textdomain_widgets_extended');

function register_widgets_extended() {
    require_once("recent_posts_widget.class.php");
    require_once('pages_widget.class.php');
    require_once('search_widget.class.php');
    require_once('nav_menu_widget.class.php');
    
    unregister_widget("WP_Widget_Recent_Posts");
    register_widget("Recent_Posts_Widget");

    unregister_widget("WP_Widget_Pages");
    register_widget("Pages_Widget");
    
    unregister_widget("WP_Widget_Search");
    register_widget("Search_Widget");
    
    unregister_widget("WP_Nav_Menu_Widget");
    register_widget("Nav_Menu_Widget");
}

add_action('widgets_init', 'register_widgets_extended');

?>
