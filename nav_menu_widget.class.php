<?php

// Exit if accessed directly
if( !defined( 'ABSPATH' ) ) exit;

class Sub_Menu_Walker extends Walker_Nav_Menu {
    function walk( $elements, $max_depth ) {
        
		$args = array_slice(func_get_args(), 2);
		$output = '';

		if ($max_depth < -1) //invalid parameter
			return $output;

		if (empty($elements)) //nothing to walk
			return $output;

		$id_field = $this->db_fields['id'];
		$parent_field = $this->db_fields['parent'];

		// flat display
		if ( -1 == $max_depth ) {
			$empty_array = array();
			foreach ( $elements as $e )
				$this->display_element( $e, $empty_array, 1, 0, $args, $output );
			return $output;
		}

		/*
		 * need to display in hierarchical order
		 * separate elements into two buckets: top level and children elements
		 * children_elements is two dimensional array, eg.
		 * children_elements[10][] contains all sub-elements whose parent is 10.
		 */
		$top_level_elements = array();
		$children_elements  = array();
		foreach ( $elements as $e) {
			if ( 0 == $e->$parent_field )
				$top_level_elements[] = $e;
			else
				$children_elements[ $e->$parent_field ][] = $e;
		}

		/*
		 * when none of the elements is top level
		 * assume the first one must be root of the sub elements
		 */
		if ( empty($top_level_elements) ) {

			$first = array_slice( $elements, 0, 1 );
			$root = $first[0];

			$top_level_elements = array();
			$children_elements  = array();
			foreach ( $elements as $e) {
				if ( $root->$parent_field == $e->$parent_field )
					$top_level_elements[] = $e;
				else
					$children_elements[ $e->$parent_field ][] = $e;
			}
		}
        
        // find sub menu start item
        $start_item = $args[0]->start_item;
        if ($args[0]->sub_menu == 1) {
            foreach ($elements as $e) {
                if ($e->current) {
                    $ancestors = array();
                    $menu_parent = $e->menu_item_parent;
                    while ( $menu_parent) {
                            $ancestors[] = (int) $menu_parent;
                            $temp_menu_paret = get_post_meta($menu_parent, '_menu_item_menu_item_parent', true);
                            $menu_parent = $temp_menu_paret;
                    }
                    if (count($ancestors) >= $args[0]->start_depth)
                        $start_item = $ancestors[count($ancestors)-$args[0]->start_depth];
                    elseif ($args[0]->start_depth == count($ancestors)+1)
                        $start_item = $e->ID;
                    break;
                }
            }
            if (!$start_item)
                return $output;
        }
        
        // top_level_elements are found sub menu start item children
        if ($start_item) {
            if (!isset($children_elements[$start_item]))
                return $output;
            
            $top_level_elements = $children_elements[$start_item];
        }
        
        foreach ( $top_level_elements as $e ) {
            $this->display_element( $e, $children_elements, $max_depth, 0, $args, $output );
        }
        
        return $output;
    }
}

class Nav_Menu_Widget extends WP_Nav_Menu_Widget {

	function widget($args, $instance) {
		// Get menu
		$nav_menu = ! empty( $instance['nav_menu'] ) ? wp_get_nav_menu_object( $instance['nav_menu'] ) : false;
        $sub_menu = !empty($instance['sub_menu']) ? absint($instance['sub_menu']) : 0;
        $start_item = !empty($instance['start_item']) ? absint($instance['start_item']) : 0;
        $start_depth = !empty($instance['start_depth']) ? absint($instance['start_depth']) : 0;
        $walker = $sub_menu ? new Sub_Menu_Walker : new Walker_Nav_Menu;

		if ( !$nav_menu )
			return;

		$instance['title'] = apply_filters( 'widget_title', empty( $instance['title'] ) ? '' : $instance['title'], $instance, $this->id_base );

		echo $args['before_widget'];

		if ( !empty($instance['title']) )
			echo $args['before_title'] . $instance['title'] . $args['after_title'];

		wp_nav_menu( array( 'fallback_cb' => '', 'menu' => $nav_menu, 'walker' => $walker, 'sub_menu' => $sub_menu, 'start_item' => $start_item, 'start_depth' => $start_depth ) );

		echo $args['after_widget'];
	}

	function update( $new_instance, $old_instance ) {
		$instance['title'] = strip_tags( stripslashes($new_instance['title']) );
		$instance['nav_menu'] = (int) $new_instance['nav_menu'];
		$instance['sub_menu'] = (int) $new_instance['sub_menu'];
        $instance['start_depth'] = $instance['sub_menu'] == 1 ? absint( $new_instance['start_depth'] ) : 0;
        $instance['start_item'] = $instance['sub_menu'] == 2 ? absint( $new_instance['start_item'] ) : 0;
		return $instance;
	}

	function form( $instance ) {
		$title = isset( $instance['title'] ) ? $instance['title'] : '';
		$nav_menu = isset( $instance['nav_menu'] ) ? $instance['nav_menu'] : '';
        $sub_menu = isset( $instance['sub_menu'] ) ? (int) $instance['sub_menu'] : 0;
        $start_item = isset( $instance['start_item'] ) ? (int) $instance['start_item'] : 0;
        $start_depth = isset($instance['start_depth']) ? absint($instance['start_depth']) : 0;

		// Get menus
		$menus = get_terms( 'nav_menu', array( 'hide_empty' => false ) );

		// If no menus exists, direct the user to go and create some.
		if ( !$menus ) {
			echo '<p>'. sprintf( __('No menus have been created yet. <a href="%s">Create some</a>.'), admin_url('nav-menus.php') ) .'</p>';
			return;
		}
		?>
		<p>
			<label for="<?php echo $this->get_field_id('title'); ?>"><?php _e('Title:') ?></label>
			<input type="text" class="widefat" id="<?php echo $this->get_field_id('title'); ?>" name="<?php echo $this->get_field_name('title'); ?>" value="<?php echo $title; ?>" />
		</p>
		<p>
			<label for="<?php echo $this->get_field_id('nav_menu'); ?>"><?php _e('Select Menu:'); ?></label>
			<select id="<?php echo $this->get_field_id('nav_menu'); ?>" name="<?php echo $this->get_field_name('nav_menu'); ?>">
		<?php
			foreach ( $menus as $menu ) {
				echo '<option value="' . $menu->term_id . '"'
					. selected( $nav_menu, $menu->term_id, false )
					. '>'. $menu->name . '</option>';
			}
		?>
			</select>
		</p>
		<p><label for="<?php echo $this->get_field_id('sub_menu'); ?>"><?php _e('Display:'); ?></label>
		<select name="<?php echo $this->get_field_name('sub_menu'); ?>" id="<?php echo $this->get_field_id('sub_menu'); ?>">
			<option value="0"<?php selected( $sub_menu, 0 ); ?>><?php _e('All'); ?></option>
			<option value="1"<?php selected( $sub_menu, 1 ); ?>><?php _e('Related submenu'); ?></option>
            <option value="2"<?php selected( $sub_menu, 2 ); ?>><?php _e('Specific submenu'); ?></option>
		</select>
		</p>
		<p id="cont_<?php echo $this->get_field_id('start_depth'); ?>"<?php echo $sub_menu != 1 ? ' style="display:none"' : ''?>>
            <label for="<?php echo $this->get_field_id('start_depth'); ?>"><?php _e('Starting depth:'); ?></label>
            <input id="<?php echo $this->get_field_id('start_depth'); ?>" name="<?php echo $this->get_field_name('start_depth'); ?>" type="number" value="<?php echo $start_depth; ?>" class="small-text" />
		</p>
		<p id="cont_<?php echo $this->get_field_id('start_item'); ?>"<?php echo $sub_menu != 2 ? ' style="display:none"' : ''?>>
            <label for="<?php echo $this->get_field_id('start_item'); ?>"><?php _e('Starting item:'); ?></label>
            <select name="<?php echo $this->get_field_name('start_item'); ?>" id="<?php echo $this->get_field_id('start_item'); ?>" class="widefat">
                <?php 
                $menu_id = ( $nav_menu ) ? $nav_menu : $menus[0]->term_id;
                $menu_items = wp_get_nav_menu_items($menu_id); 
                foreach ( $menu_items as $menu_item ) {
                    echo '<option value="'.$menu_item->ID.'"'.selected( $start_item, $menu_item->ID ).'>'.$menu_item->title.'</option>';
                }
                ?>		
            </select>
		</p>
        <script type="text/javascript">
            jQuery(document).ready(function($) {
                $("#<?php echo $this->get_field_id('sub_menu'); ?>").change(function() {
                    if (this.value == 1)
                        $("#cont_<?php echo $this->get_field_id('start_depth'); ?>").show();
                    else
                        $("#cont_<?php echo $this->get_field_id('start_depth'); ?>").hide();
                    if (this.value == 2)
                        $("#cont_<?php echo $this->get_field_id('start_item'); ?>").show();
                    else
                        $("#cont_<?php echo $this->get_field_id('start_item'); ?>").hide();
                });
            });
        </script>
		<?php
	}
}

?>
